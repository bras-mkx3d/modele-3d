# Bras MKX3D - Modèle 3D

- Format : SolidWorks, FreeCAD et STEP
- Licence : Creative Commons BY-NC-SA V4
- Dépôt : https://forge.apps.education.fr/bras-mkx3d/modele-3d

## Auteurs :
 - Pascal Dalmeida [linkedin](https://www.linkedin.com/in/pascal-dalmeida-0a1025153)
 - Enric Gómez [linkedin](https://www.linkedin.com/in/mezarch)
  - Philippe de Poumayrac de Masredon [linkedin](https://www.linkedin.com/in/philippe-de-poumayrac )
 
![Capture écran FreeCad](img/bras_mkx3d.png)
![Photo](img/bras_mkx3d-photo.png)
